# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/oracle/oci" {
  version     = "4.117.0"
  constraints = "4.117.0"
  hashes = [
    "h1:5vgukiMGLZkk34hDVu+uXAXNC1cxyubbfBU5Oj09Vig=",
    "zh:02dfbc8fd0c499f7f8b8d9d24f9c2da8d3bee554d59eaf8ecbb28c90a239e7ba",
    "zh:099f3887c54e40c697b2a5eef6b721030678610df2901a457ad757c3c4b5698c",
    "zh:1af3838edc64bd8b317aad83af21893d8615fb15840a4425801d544c85084aff",
    "zh:3b19dcda37c7962316d08025a675e3a2acae514c02e06f5aba617bf0fb71bc8e",
    "zh:41be232377f403e33855d5a7d7a85d93e3d8ab8878386c78095f2423bb626081",
    "zh:460583c8ee8b356214c73fd3b7ac45fd9df7a4c31c433d5fdb57eae939fca6b1",
    "zh:5eeb02436a27897882fe1362f3ff2b0920c0fc5b2b3dbad4807eac09c775136e",
    "zh:8f52dc09b7065133ac2da075b6b45646f718ead45ec236477afdf0b550df10fa",
    "zh:9b12af85486a96aedd8d7984b0ff811a4b42e3d88dad1a3fb4c0b580d04fa425",
    "zh:a4ccbdab9a8526f0a945cc64a7395e52e380ba300a33ec6b9aafb67104955442",
    "zh:ac421603c3713413eec40267eee1671925780b308d6f7434656ed77bf71b248e",
    "zh:b1703c524a6c78d6f25e34b986d0d8eaa0538d9543d48b91845ac09932e92c1d",
    "zh:c06cf73c087445b62cc150d2afc1d7f73ef668fb05b624147329b2c17c59930f",
    "zh:cf262c71b5b1a8e36aeaedf4bcee7d0a83b475eeb3fba5cbfdb9c96616563a50",
    "zh:f5edfdb91d36a18aec91d5f859d05ab106e5802d0ce1496259b9eae0587181ef",
  ]
}
